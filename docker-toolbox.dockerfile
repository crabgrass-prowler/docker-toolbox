# Pushed up to: https://hub.docker.com/r/comradesmith/docker-toolbox
# And:          registry.digitalocean.com/cam-tainers/docker-toolbox

FROM debian:stable-slim

RUN apt update && apt install apt-transport-https curl gnupg2 -y
RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
RUN echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list
RUN apt update && apt install \
  ruby ruby-kubeclient ruby-redis ruby-sqlite3 \
  python3 python3-ipython python3-kubernetes python3-pip python3-redis \
  dnsutils netcat-openbsd nmap speedtest-cli telnet traceroute \
  postgresql-client sqlite3 \
  redis-tools \
  ack gron hexedit jq vim \
  diceware \
  awscli kubectl \
  -y \
&& true

RUN apt clean && apt autoclean && apt autoremove

RUN curl -LO https://github.com/digitalocean/doctl/releases/download/v1.76.2/doctl-1.76.2-linux-amd64.tar.gz \
  && tar xvf doctl-1.76.2-linux-amd64.tar.gz \
  && mv doctl /bin/ \
&& true


RUN pip3 install yq

ENTRYPOINT ["sleep", "infinity"]
